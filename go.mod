module lemon_go

go 1.14

require (
	github.com/golang/protobuf v1.4.0
	github.com/nats-io/nats.go v1.9.2
	google.golang.org/protobuf v1.21.0
)
