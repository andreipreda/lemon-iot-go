package main

import (
	"fmt"
	nats "github.com/nats-io/nats.go"
	"github.com/nats-io/nats.go/encoders/protobuf"
	"lemon_go/cap_temp"
	"os"
	"sync"
	"time"
)

func main() {

	fmt.Println("Connecting to NATS and listening")
	nc, _ := nats.Connect(os.Getenv("NATS_HOSTNAME"))

	// Wrap the connection into a
	ec, errEn := nats.NewEncodedConn(nc, protobuf.PROTOBUF_ENCODER)
	if errEn != nil {
		fmt.Println(errEn)
	}
	defer ec.Close()

	wg := sync.WaitGroup{}
	wg.Add(1)

	// Subscribe
	if _, err := ec.Subscribe(os.Getenv("NATS_SUBJECT"), func(msg *cap_temp.CapTemp) {
		fmt.Printf("Received %v capacitive, temperature %v at %s\n",
			msg.GetCapacitive(),
			msg.GetTemperature(),
			time.Unix(int64(msg.GetTimestamp()), 0))
	}); err != nil {
		fmt.Printf(err.Error())
	}

	// Wait for a message to come in
	wg.Wait()
}
